#include <stdio.h>
#include <allegro5/allegro.h>
#include "function.h"

char tmp[RES_X*RES_Y*3];

int main(int argc, char **argv){
  double scale = 4.0 / RES_X;
  double start_x = -2.0;
  double start_y = -1.0;
  ALLEGRO_DISPLAY *display = NULL;
  ALLEGRO_KEYBOARD_STATE keyboard;
  ALLEGRO_LOCKED_REGION *bitmap;
  ALLEGRO_MOUSE_STATE mouse;
  ALLEGRO_BITMAP *screen;
  int mouse_flags = 0;
  if(!al_init()) {
    fprintf(stderr, "failed to initialize allegro!\n");
    return -1;
  }
	al_set_new_display_option(ALLEGRO_COLOR_SIZE, 24, ALLEGRO_REQUIRE);

  display = al_create_display(RES_X, RES_Y);
  if(!display) {
	 fprintf(stderr, "failed to create display!\n");
	 return -1;
  }
	if(!al_install_keyboard()) {
	 fprintf(stderr, "failed to init keyboard\n");
   return -1;
 }
	 if(!al_install_mouse()) {
		 fprintf(stderr, "failed to install mouse");
		 return -1;
	 }
	 screen = al_create_bitmap(RES_X, RES_Y);
	 al_set_target_bitmap(screen);
   al_clear_to_color(al_map_rgb(0,0,0));
	 if(screen == NULL) return -1;
   al_flip_display();
	 bitmap = al_lock_bitmap(screen, ALLEGRO_PIXEL_FORMAT_RGB_888, ALLEGRO_LOCK_READWRITE);
   unsigned char *ptr = (unsigned char*)(bitmap->data);
   ptr = ptr - RES_X*(RES_Y-1)*3;
   my_function(ptr, RES_X, RES_Y, scale, start_x, start_y);
	 al_unlock_bitmap(screen);
	 al_set_target_bitmap(al_get_backbuffer(display));
   al_draw_bitmap(screen, 0, 0, 0);
	 al_flip_display();
   while(!al_key_down(&keyboard, ALLEGRO_KEY_ESCAPE)) {
		 al_get_keyboard_state(&keyboard);
		 al_get_mouse_state(&mouse);
		 if(mouse.buttons == 0) {
			 if(mouse_flags == 1 || mouse_flags == 2) {
          if (mouse_flags == 1) {

            start_x = start_x + (double)(scale * mouse.x - scale*RES_X/4.0);
            start_y = start_y + (double)(scale * (RES_Y - mouse.y) - scale * RES_Y/4.0);
            scale  = scale / 2.0;
          } else if(mouse_flags == 2) {
            start_x = start_x + (double)(scale * mouse.x - scale * RES_X);
            start_y = start_y + (double)(scale * (RES_Y - mouse.y) - scale * RES_Y);
            scale = scale * 2.0;
          }
					bitmap = al_lock_bitmap(screen, ALLEGRO_PIXEL_FORMAT_RGB_888, ALLEGRO_LOCK_READWRITE);
					ptr = (unsigned char*)(bitmap->data);
					ptr = ptr - RES_X*(RES_Y-1)*3;
          my_function(ptr, RES_X, RES_Y, scale, start_x, start_y);
					al_unlock_bitmap(screen);
					al_draw_bitmap(screen, 0, 0, 0);
					al_flip_display();
          printf("x=%lf; y=%lf; scale=%lf;\n", start_x, start_y, scale);
			 }
			 mouse_flags = 0;
		 }
		 if(mouse.buttons != 0) {
			 mouse_flags = mouse.buttons;
		 }
	 }

   al_destroy_display(display);

   return 0;
}
