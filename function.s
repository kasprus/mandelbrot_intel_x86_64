section .text
global my_function
my_function:
;prolog
push rbp
mov rbp, rsp

push r12
push r13
push r14
push r15
;rdi - buffer
;rsi - x
;rdx - y
;rcx - scale
;r8 - start_x
;r9 - start_y
movq rcx, xmm0
movq r8, xmm1
movq r9, xmm2

xor r10, r10 ;current line
xor r11, r11 ;offset
mov r14, r9; current y real value
y_iteration_loop:
;iteration over lines
cmp r10, rdx
je end_of_y_iteration_loop

xor r12, r12 ;current pixel in line
mov r13, r8 ; current x real value


x_iteration_loop:
;iteration over pixels in lines
cmp r12, rsi
je end_of_x_iteration_loop

push r13
push r14

xor r15, r15; iterator
begin_of_mandelbrot_loop:
cmp r15, 500;
je end_of_mandelbrot_loop


movq xmm0, r13
movq xmm1, r14

mulsd xmm0, xmm0
mulsd xmm1, xmm1
addsd xmm0, xmm1
mov rax, __float64__(4.0)
movq xmm1, rax


comisd xmm0, xmm1
jae colors

movq xmm3, r13
movq xmm0, r13
movq xmm1, r14
movq xmm2, [rsp + 8]
mulsd xmm0, xmm0
mulsd xmm1, xmm1
subsd xmm0, xmm1
addsd xmm0, xmm2
movq r13, xmm0

movq xmm0, r14
mov rax, __float64__(2.0)
movq xmm1, rax
mulsd xmm0, xmm1
mulsd xmm0, xmm3
addsd xmm0, [rsp]
movq r14, xmm0

inc r15

jmp begin_of_mandelbrot_loop
end_of_mandelbrot_loop:
mov byte [rdi + r11],0
mov byte [rdi + r11 + 1], 0
mov byte [rdi + r11 + 2], 0
jmp end_colors

colors:
lea rax, [r15 * 9]
mov byte [rdi + r11],al
mov byte [rdi + r11 + 2], 160
lea rax, [rax*2]
mov byte [rdi + r11 + 1], al

end_colors:
pop r14
pop r13


add r11, 3
inc r12
movq xmm0, r13
movq xmm1, rcx
addsd xmm0, xmm1
movq r13, xmm0
jmp x_iteration_loop

end_of_x_iteration_loop:
inc r10
movq xmm0, r14
movq xmm1, rcx
addsd xmm0, xmm1
movq r14, xmm0
jmp y_iteration_loop

end_of_y_iteration_loop:

pop r15
pop r14
pop r13
pop r12
;epilog
mov rsp, rbp
pop rbp
ret
