CC=gcc
LDFLAGS=-L/usr/lib -lallegro
INCLUDE=-I. -I/usr/include/allegro5

all: main.o function.o
	$(CC) -o program function.o main.o $(INCLUDE) $(LDFLAGS)
	rm *.o

function.o: function.o
	nasm -f elf64 -o function.o function.s
main.o: main.c function.h
	$(CC) -c main.c

#function.o: function.o
#	$(CC) -c function.c

clean:
	rm -f *.o
